// A simple client that replies "pong" when it hears "ping".

use actix::{Actor, Addr, AsyncContext, Context, Handler, System};
use actix_irc::irc::client::data::config::Config;
use actix_irc::{Event, IrcClientActor, SendMessage};
use irc::proto::command::Command;

// Define our actor type ...
struct Ping {
    // It contains the address of the IRC client for sending messages back.
    client: Addr<IrcClientActor<Ping>>,
}

impl Ping {
    // Constructor:
    fn start(config: Config) -> Addr<Ping> {
        Ping::create(|ctx| Ping {
            client: IrcClientActor::start(config, ctx.address()),
        })
    }
}

impl Actor for Ping {
    type Context = Context<Self>;
}

// Our handler implementatation that filters by messages that match something like "PRIVMSG * :ping".
impl Handler<Event> for Ping {
    type Result = ();

    fn handle(&mut self, event: Event, _ctx: &mut Self::Context) -> Self::Result {
        match event {
            Event::Received(msg) => match &msg.command {
                Command::PRIVMSG(_target, message) => {
                    if message.eq_ignore_ascii_case("ping") {
                        // We have been summoned!
                        self.client.do_send(SendMessage(Command::PRIVMSG(
                            msg.response_target().unwrap().to_string(),
                            "Pong!".to_owned(),
                        )));
                    }
                }
                _ => {}
            },
            Event::Error(err) => {
                println!("{}", err);
                System::current().stop();
            }
        }
    }
}

fn main() {
    // Set up configuration.
    // (modify this to point to the server and channel of your choosing)
    let config = Config {
        nickname: Some("actix-irc".to_owned()),
        server: Some("localhost".to_owned()),
        port: Some(6697),
        use_ssl: Some(true),
        channels: Some(vec!["#actix-irc".to_owned()]),
        ..Config::default()
    };

    // Within the Actix runtime ...
    System::run(|| {
        // Start our Ping actor.
        Ping::start(config);
    })
    .unwrap();
}
