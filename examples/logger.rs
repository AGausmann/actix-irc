use actix::{Actor, Context, Handler, System};
use actix_irc::irc::client::data::config::Config;
use actix_irc::{Event, IrcClientActor};

// Define our logger type, let it handle events from the IRC actor and print them.
struct Logger;

impl Actor for Logger {
    type Context = Context<Self>;
}

impl Handler<Event> for Logger {
    type Result = ();

    fn handle(&mut self, event: Event, _ctx: &mut Self::Context) -> Self::Result {
        match event {
            Event::Received(msg) => print!("{}", msg),
            Event::Error(err) => {
                println!("{}", err);
                System::current().stop();
            }
        }
    }
}

fn main() {
    // Set up configuration.
    // (modify this to point to the server and channel of your choosing)
    let config = Config {
        nickname: Some("actix-irc".to_owned()),
        server: Some("localhost".to_owned()),
        port: Some(6697),
        use_ssl: Some(true),
        channels: Some(vec!["#actix-irc".to_owned()]),
        ..Config::default()
    };

    // Within the Actix runtime ...
    System::run(|| {
        // Instantiate our logger.
        let logger = Logger.start();

        // Start the IRC connection, pass it the given configuration and callback.
        IrcClientActor::start(config, logger);
    })
    .unwrap();
}
