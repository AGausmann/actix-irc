# actix-irc

An actor framework for the IRC protocol.


# Examples

A simple client that replies "pong" when it hears "ping":

```no_run
use actix::{Actor, Addr, AsyncContext, Context, Handler, System};
use actix_irc::irc::client::data::config::Config;
use actix_irc::{Event, IrcClientActor, SendMessage};
use irc::proto::command::Command;

// Define our actor type ...
struct Ping {
    // It contains the address of the IRC client for sending messages back.
    client: Addr<IrcClientActor<Ping>>,
}

impl Ping {
    // Constructor:
    fn start(config: Config) -> Addr<Ping> {
        Ping::create(|ctx| Ping {
            client: IrcClientActor::start(config, ctx.address()),
        })
    }
}

impl Actor for Ping {
    type Context = Context<Self>;
}

// Our handler implementatation that filters by messages that match something like "PRIVMSG * :ping".
impl Handler<Event> for Ping {
    type Result = ();

    fn handle(&mut self, event: Event, _ctx: &mut Self::Context) -> Self::Result {
        match event {
            Event::Received(msg) => match &msg.command {
                Command::PRIVMSG(_target, message) => {
                    if message.eq_ignore_ascii_case("ping") {
                        // We have been summoned!
                        self.client.do_send(SendMessage(Command::PRIVMSG(
                            msg.response_target().unwrap().to_string(),
                            "Pong!".to_owned(),
                        )));
                    }
                }
                _ => {}
            },
            Event::Error(err) => {
                println!("{}", err);
                System::current().stop();
            }
        }
    }
}

fn main() {
    // Set up configuration.
    // (modify this to point to the server and channel of your choosing)
    let config = Config {
        nickname: Some("actix-irc".to_owned()),
        server: Some("localhost".to_owned()),
        port: Some(6697),
        use_ssl: Some(true),
        channels: Some(vec!["#actix-irc".to_owned()]),
        ..Config::default()
    };

    // Within the Actix runtime ...
    System::run(|| {
        // Start our Ping actor.
        Ping::start(config);
    })
    .unwrap();
}
```

## License

Copyright (c) 2019 Adam Gausmann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
